# Copyright (C) 2023 This file is copyright:
# This file is distributed under the same license as the kate package.
#
# SPDX-FileCopyrightText: 2023 Enol P. <enolp@softastur.org>
msgid ""
msgstr ""
"Project-Id-Version: kate\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-03-04 00:39+0000\n"
"PO-Revision-Date: 2023-11-05 01:40+0100\n"
"Last-Translator: Enol P. <enolp@softastur.org>\n"
"Language-Team: \n"
"Language: ast\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.08.2\n"

#: kateconsole.cpp:56
#, kde-format
msgid "You do not have enough karma to access a shell or terminal emulation"
msgstr ""

#: kateconsole.cpp:104 kateconsole.cpp:134 kateconsole.cpp:702
#, kde-format
msgid "Terminal"
msgstr "Terminal"

#: kateconsole.cpp:143
#, kde-format
msgctxt "@action"
msgid "&Pipe to Terminal"
msgstr ""

#: kateconsole.cpp:147
#, kde-format
msgctxt "@action"
msgid "S&ynchronize Terminal with Current Document"
msgstr ""

#: kateconsole.cpp:151
#, kde-format
msgctxt "@action"
msgid "Run Current Document"
msgstr ""

#: kateconsole.cpp:156 kateconsole.cpp:520
#, kde-format
msgctxt "@action"
msgid "S&how Terminal Panel"
msgstr ""

#: kateconsole.cpp:162
#, kde-format
msgctxt "@action"
msgid "&Focus Terminal Panel"
msgstr ""

#: kateconsole.cpp:168
#, kde-format
msgctxt "@action"
msgid "&Split Terminal Vertically"
msgstr ""

#: kateconsole.cpp:173
#, kde-format
msgctxt "@action"
msgid "&Split Terminal Horizontally"
msgstr ""

#: kateconsole.cpp:178
#, fuzzy, kde-format
#| msgid "Terminal"
msgctxt "@action"
msgid "&New Terminal Tab"
msgstr "Terminal"

#: kateconsole.cpp:319
#, kde-format
msgid ""
"Konsole not installed. Please install konsole to be able to use the terminal."
msgstr ""

#: kateconsole.cpp:395
#, kde-format
msgid ""
"Do you really want to pipe the text to the console? This will execute any "
"contained commands with your user rights."
msgstr ""

#: kateconsole.cpp:396
#, kde-format
msgid "Pipe to Terminal?"
msgstr ""

#: kateconsole.cpp:397
#, kde-format
msgid "Pipe to Terminal"
msgstr ""

#: kateconsole.cpp:425
#, kde-format
msgid "Sorry, cannot cd into '%1'"
msgstr ""

#: kateconsole.cpp:461
#, kde-format
msgid "Not a local file: '%1'"
msgstr ""

#: kateconsole.cpp:494
#, kde-format
msgid ""
"Do you really want to Run the document ?\n"
"This will execute the following command,\n"
"with your user rights, in the terminal:\n"
"'%1'"
msgstr ""

#: kateconsole.cpp:501
#, kde-format
msgid "Run in Terminal?"
msgstr ""

#: kateconsole.cpp:502
#, kde-format
msgid "Run"
msgstr ""

#: kateconsole.cpp:517
#, kde-format
msgctxt "@action"
msgid "&Hide Terminal Panel"
msgstr ""

#: kateconsole.cpp:528
#, kde-format
msgid "Defocus Terminal Panel"
msgstr ""

#: kateconsole.cpp:529 kateconsole.cpp:530
#, kde-format
msgid "Focus Terminal Panel"
msgstr ""

#: kateconsole.cpp:635
#, kde-format
msgid ""
"&Automatically synchronize the terminal with the current document when "
"possible"
msgstr ""

#: kateconsole.cpp:639 kateconsole.cpp:660
#, kde-format
msgid "Run in terminal"
msgstr ""

#: kateconsole.cpp:641
#, kde-format
msgid "&Remove extension"
msgstr ""

#: kateconsole.cpp:646
#, kde-format
msgid "Prefix:"
msgstr "Prefixu:"

#: kateconsole.cpp:654
#, kde-format
msgid "&Show warning next time"
msgstr ""

#: kateconsole.cpp:656
#, kde-format
msgid ""
"The next time '%1' is executed, make sure a warning window will pop up, "
"displaying the command to be sent to terminal, for review."
msgstr ""

#: kateconsole.cpp:667
#, kde-format
msgid "Set &EDITOR environment variable to 'kate -b'"
msgstr ""

#: kateconsole.cpp:670
#, kde-format
msgid ""
"Important: The document has to be closed to make the console application "
"continue"
msgstr ""

#: kateconsole.cpp:673
#, kde-format
msgid "Hide Konsole on pressing 'Esc'"
msgstr ""

#: kateconsole.cpp:676
#, kde-format
msgid ""
"This may cause issues with terminal apps that use Esc key, for e.g., vim. "
"Add these apps in the input below (Comma separated list)"
msgstr ""

#: kateconsole.cpp:707
#, kde-format
msgid "Terminal Settings"
msgstr ""

#. i18n: ectx: Menu (tools)
#: ui.rc:6
#, kde-format
msgid "&Tools"
msgstr "&Ferramientes"
